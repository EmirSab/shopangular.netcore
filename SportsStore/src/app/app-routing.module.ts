import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductSelectionComponent } from './store/productSelection.component';
import { CartDetailComponent } from './store/cartDetail.component';
import { CheckoutDetailsComponent } from './store/checkout/checkoutDetails.component';
import { CheckoutPaymentComponent } from './store/checkout/checkoutPayment.component';
import { CheckoutSummaryComponent } from './store/checkout/checkoutSummary.component';
import { OrderConfirmationComponent } from './store/checkout/orderConfirmation.component';
// import { ProductTableComponent } from './structure/productTable.component';
// import { ProductDetailComponent } from './structure/productDetail.component';

// 20. Dodati rute ->app.component.html
// 20.5 Dodati detail rutu sa id ->productDetail.component.ts
// 23.2 zakomentarisati product table i detail a dodati product selection ->Startup.cs
const routes: Routes = [
  //#region 33.9 DOdati novu rutu za admin module ->productSelection.component.html
  {
    path: 'admin',
    loadChildren: () =>
    import('./admin/admin.module').then(module => module.AdminModule),
    },
  //#endregion
  //#region 31.14 dodati nove rute ->Startup.cs
  { path: 'checkout/step1', component: CheckoutDetailsComponent },
  { path: 'checkout/step2', component: CheckoutPaymentComponent },
  { path: 'checkout/step3', component: CheckoutSummaryComponent },
  { path: 'checkout/confirmation', component: OrderConfirmationComponent },
  { path: 'checkout', redirectTo: '/checkout/step1', pathMatch: 'full' },
  //#endregion
  // 28.3 Dodati rutu za cart detalje -Startup.cs
  { path: 'cart', component: CartDetailComponent },
  // { path: 'table', component: ProductTableComponent },
  // { path: 'detail', component: ProductDetailComponent },
  // { path: 'detail/:id', component: ProductDetailComponent },
  // { path: '', component: ProductTableComponent }
  // 24.5 dodati rutu za kategorije ->categoryFilter.component.ts
  // 25 Dodati pathove za paginaciju ->configClasses.repository.ts
  { path: 'store/:category/:page', component: ProductSelectionComponent },
  { path: 'store/:categoryOrPage', component: ProductSelectionComponent },
  { path: 'store/:category', component: ProductSelectionComponent },
  { path: 'store', component: ProductSelectionComponent },
  { path: '', redirectTo: '/store', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
