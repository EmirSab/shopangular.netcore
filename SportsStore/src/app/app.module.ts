import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ModelModule } from './models/model.module';
// import { ProductTableComponent } from './structure/productTable.component';
// import { CategoryFilterComponent } from './structure/categoryFilter.component';
// import { ProductDetailComponent } from './structure/productDetail.component';
import { FormsModule } from '@angular/forms';
import { StoreModule } from './store/store.module';
// 18.5 dodati nove komponente ProductTableComponent, CategoryFilterComponent ->app.component.html
// 19.2 Dodati detail u module ->app.component.html
// 23.1 Zakomentarisati ProductTableComponent CategoryFilterComponen ProductDetailComponent
// a dodati store module i forms module ->app-routing.module.ts
// 36.1 Dodati modul za errore i interceptor->app.component.ts
import { ExternalService } from './external.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ErrorHandlerService } from './errorHandler.service';
@NgModule({
  declarations: [
    AppComponent
    // ProductTableComponent,
    // CategoryFilterComponent,
    // ProductDetailComponent
  ],
  imports: [
    BrowserModule, AppRoutingModule, ModelModule, FormsModule, StoreModule
  ],
  providers: [ExternalService, ErrorHandlerService,
    { provide: HTTP_INTERCEPTORS,
    useExisting: ErrorHandlerService, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
