import { Component } from '@angular/core';
import { AuthenticationService } from './authentication.service';
@Component({
    templateUrl: 'authentication.component.html'
})
// 38.10 Dodati authentikaciju prilikom logina ->authentication.component.html
export class AuthenticationComponent {
    constructor(public authService: AuthenticationService) { }
    // tslint:disable-next-line: no-inferrable-types
    showError: boolean = false;
    // tslint:disable-next-line: typedef
    login() {
        this.showError = false;
        this.authService.login().subscribe(result => {
            this.showError = !result;
        });
    }
}
