import { NgModule } from '@angular/core';
import { Repository } from './repository';
import { HttpClientModule } from '@angular/common/http';
import { NavigationService } from './navigation.service';
import { Cart } from './cart.model';
import { Order } from './order.model';
// 27.1 Dodati cart module ->productList.component.ts
// 31.4 dodati order u module ->checkoutDetails.component.t
@NgModule({
    imports: [HttpClientModule],
    providers: [Repository, NavigationService, Cart, Order]
})
// 24.4 dodati navigation servis -> app-routing.module.ts
// za problem experimental decorators https://stackoverflow.com/questions/38271273/experimental-decorators-warning-in-typescript-compilation
export class ModelModule { }
