//#region 27 napraviti card model ->model.module.ts
import { Injectable } from '@angular/core';
import { Product } from './product.model';
import { Repository } from './repository';
@Injectable()
export class Cart {
    selections: ProductSelection[] = [];
    // tslint:disable-next-line: no-inferrable-types
    itemCount: number = 0;
    // tslint:disable-next-line: no-inferrable-types
    totalPrice: number = 0;

    //#region 29.4 dodati konstuktor za session storage
    constructor(private repo: Repository) {
        repo.getSessionData<ProductSelection[]>('cart').subscribe(cartData => {
            if (cartData != null) {
                cartData.forEach(item => this.selections.push(item));
                this.update(false);
            }
        });
    }
    //#endregion
    // tslint:disable-next-line: typedef
    addProduct(product: Product) {
        // tslint:disable-next-line: prefer-const
        let selection = this.selections
            .find(ps => ps.productId === product.productId);
        if (selection) {
            selection.quantity++;
        } else {
            this.selections.push(new ProductSelection(this,
                product.productId, product.name,
                product.price, 1));
        }
        this.update();
    }
    // tslint:disable-next-line: typedef
    updateQuantity(productId: number, quantity: number) {
        if (quantity > 0) {
            // tslint:disable-next-line: prefer-const
            let selection = this.selections.find(ps => ps.productId === productId);
            if (selection) {
                selection.quantity = quantity;
            }
        } else {
            // tslint:disable-next-line: prefer-const
            let index = this.selections.findIndex(ps => ps.productId === productId);
            if (index !== -1) {
                this.selections.splice(index, 1);
            }
            this.update();
        }
    }
    // tslint:disable-next-line: typedef
    clear() {
        this.selections = [];
        this.update();
    }

    //#region 29.4 dodati dio za session storage cart ->Order.cs
    // tslint:disable-next-line: typedef
    update(storeData: boolean = true) {
        this.itemCount = this.selections.map(ps => ps.quantity)
            .reduce((prev, curr) => prev + curr, 0);
        this.totalPrice = this.selections.map(ps => ps.price * ps.quantity)
            .reduce((prev, curr) => prev + curr, 0);
        if (storeData) {
            this.repo.storeSessionData('cart', this.selections.map(s => {
                return {
                    productId: s.productId, name: s.name,
                    price: s.price, quantity: s.quantity
                };
            }));
        }
        //#endregion
        
        // tslint:disable-next-line: typedef
        /*update() {
            this.itemCount = this.selections.map(ps => ps.quantity)
                .reduce((prev, curr) => prev + curr, 0);
            this.totalPrice = this.selections.map(ps => ps.price * ps.quantity)
                .reduce((prev, curr) => prev + curr, 0);
        }*/
    }
}
export class ProductSelection {
    // tslint:disable-next-line: max-line-length
    constructor(public cart: Cart, public productId?: number, public name?: string, public price?: number, private quantityValue?: number) { }
    // tslint:disable-next-line: typedef
    get quantity() {
        return this.quantityValue;
    }
    set quantity(newQuantity: number) {
        this.quantityValue = newQuantity;
        this.cart.update();
    }
}
//#endregion
