// 12 Dodati klasu za filtere ->repository.ts
export class Filter {
    category?: string;
    search?: string;
    related = false;
    // tslint:disable-next-line: typedef
    reset() {
    this.category = this.search = null;
    this.related = false;
    }
}

//#region 25.1 dodati paginaciju ->repository.ts
export class Pagination {
    // tslint:disable-next-line: no-inferrable-types
    productsPerPage: number = 4;
    currentPage = 1;
    }
//#endregion
