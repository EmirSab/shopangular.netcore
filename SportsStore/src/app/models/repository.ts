import { Product } from './product.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Filter, Pagination } from './configClasses.repository';
import { Supplier } from './supplier.model';
import { Observable } from 'rxjs';
import { Order, OrderConfirmation } from './order.model';
// 1. Dodati getProduct u repository
// 8. Dodati metod getProduccts(), prepraviti propertije i metode ispod ->app.component.ts
// 13. Dodati filtere
// 14.5 Dodati CreateProduct i supplier na frontend ->app.component.ts
const productsUrl = '/api/products';
const suppliersUrl = '/api/suppliers';
// 29.3 Slanje i primanje podataka za cart ->cart.model.ts
const sessionUrl = '/api/session';
// 31.3 prije ovog dodati kontroller OrderValuesController
const ordersUrl = '/api/orders';
// 24.1 Dodati propertije i logiku za filter ->navigation.service.ts
type productsMetadata = {
    data: Product[],
    categories: string[];
};
@Injectable()
export class Repository {
    product: Product;
    products: Product[];
    // 14.5
    suppliers: Supplier[] = [];
    // 13
    filter: Filter = new Filter();
    // 24.1
    categories: string[] = [];
    // 31.3
    orders: Order[] = [];
    // 25.2 dodati paginaciju u repo ->navigation.service.ts
    paginationObject = new Pagination();
    constructor(private http: HttpClient) {
        // 13
        // this.filter.category = 'soccer';
        this.filter.related = true;
        // 24.3 zakomentarisati getProducts() ->model.module.ts
        // this.getProducts();
        // 8
        // this.getProducts(true);
        // this.product = JSON.parse(document.getElementById('data').textContent);
    }
    //#region 38.8 dodati login i logut na frontend ->authentication.service.ts
    login(name: string, password: string): Observable<boolean> {
        return this.http.post<boolean>('/api/account/login', { name: name, password: password });
    }
    // tslint:disable-next-line: typedef
    logout() {
        this.http.post('/api/account/logout', null).subscribe(response => { });
    }
    //#endregion
    // tslint:disable-next-line: typedef
    getProduct(id: number) {
        this.http.get<Product>(`${productsUrl}/${id}`)
            .subscribe(p => this.product = p);
    }
    // tslint:disable-next-line: typedef
    getProducts(related = false) {
        // 13
        let url = `${productsUrl}?related=${this.filter.related}`;
        if (this.filter.category) {
            url += `&category=${this.filter.category}`;
        }
        if (this.filter.search) {
            url += `&search=${this.filter.search}`;
        }
        // 24.1
        url += '&metadata=true';
        this.http.get<productsMetadata>(url)
            .subscribe(md => {
                this.products = md.data;
                this.categories = md.categories;
            });
        // this.http.get<Product[]>(url).subscribe(prods => this.products = prods);
        // 8
        // this.http.get<Product[]>(`${productsUrl}?related=${related}`)
        // .subscribe(prods => this.products = prods);
    }

    // tslint:disable-next-line: typedef
    getSuppliers() {
        this.http.get<Supplier[]>(suppliersUrl)
            .subscribe(sups => this.suppliers = sups);
    }
    // 14.5
    // tslint:disable-next-line: typedef
    createProduct(prod: Product) {
        const data = {
            name: prod.name, category: prod.category,
            description: prod.description, price: prod.price,
            supplier: prod.supplier ? prod.supplier.supplierId : 0
        };
        this.http.post<number>(productsUrl, data)
            .subscribe(id => {
                prod.productId = id;
                this.products.push(prod);
            });
    }

    // 14.5
    // tslint:disable-next-line: typedef
    createProductAndSupplier(prod: Product, supp: Supplier) {
        const data = {
            name: supp.name, city: supp.city, state: supp.state
        };
        this.http.post<number>(suppliersUrl, data)
            .subscribe(id => {
                supp.supplierId = id;
                prod.supplier = supp;
                this.suppliers.push(supp);
                if (prod != null) {
                    this.createProduct(prod);
                }
            });
    }
    //#region Dodati update metode za product i supplier
    // 15.3 DOdati update metode za product i supplier ->app.component.ts
    // tslint:disable-next-line: typedef
    replaceProduct(prod: Product) {
        // tslint:disable-next-line: prefer-const
        let data = {
            name: prod.name, category: prod.category,
            description: prod.description, price: prod.price,
            supplier: prod.supplier ? prod.supplier.supplierId : 0
        };
        this.http.put(`${productsUrl}/${prod.productId}`, data)
            .subscribe(() => this.getProducts());
    }
    // tslint:disable-next-line: typedef
    replaceSupplier(supp: Supplier) {
        // tslint:disable-next-line: prefer-const
        let data = {
            name: supp.name, city: supp.city, state: supp.state
        };
        this.http.put(`${suppliersUrl}/${supp.supplierId}`, data)
            .subscribe(() => this.getProducts());
    }
    //#endregion Dodati update metode za product i supplier

    //#region 16.4 Dodati patch na front ->app.component.ts
    // tslint:disable-next-line: typedef
    updateProduct(id: number, changes: Map<string, any>) {
        // tslint:disable-next-line: prefer-const
        let patch = [];
        changes.forEach((value, key) =>
            // tslint:disable-next-line: object-literal-shorthand
            patch.push({ op: 'replace', path: key, value: value }));
        this.http.patch(`${productsUrl}/${id}`, patch)
            .subscribe(() => this.getProducts());
    }
    //#endregion

    //#region 17.4 dodati delete metode ->app.component.ts
    // tslint:disable-next-line: typedef
    deleteProduct(id: number) {
        this.http.delete(`${productsUrl}/${id}`)
            .subscribe(() => this.getProducts());
    }
    // tslint:disable-next-line: typedef
    deleteSupplier(id: number) {
        this.http.delete(`${suppliersUrl}/${id}`)
            .subscribe(() => {
                this.getProducts();
                this.getSuppliers();
            });
    }
    //#endregion

    //#region 29.3 Slanje i primanje podataka za cart
    // tslint:disable-next-line: typedef
    storeSessionData<T>(dataType: string, data: T) {
        return this.http.post(`${sessionUrl}/${dataType}`, data)
            .subscribe(response => { });
    }
    getSessionData<T>(dataType: string): Observable<T> {
        return this.http.get<T>(`${sessionUrl}/${dataType}`);
    }
    //#endregion

    //#region  dodati metode za proces ordera ->model.module.ts
    // tslint:disable-next-line: typedef
    getOrders() {
        this.http.get<Order[]>(ordersUrl)
            .subscribe(data => this.orders = data);
    }
    // tslint:disable-next-line: typedef
    createOrder(order: Order) {
        this.http.post<OrderConfirmation>(ordersUrl, {
            name: order.name,
            address: order.address,
            payment: order.payment,
            products: order.products
        }).subscribe(data => {
            order.orderConfirmation = data;
            order.cart.clear();
            order.clear();
        });
    }
    // tslint:disable-next-line: typedef
    shipOrder(order: Order) {
        this.http.post(`${ordersUrl}/${order.orderId}`, {})
            .subscribe(() => this.getOrders());
    }
    //#endregion

}
