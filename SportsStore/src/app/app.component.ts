import { Component } from '@angular/core';
import { Product } from './models/product.model';
import { Repository } from './models/repository';
import { Supplier } from './models/supplier.model';
import { ErrorHandlerService } from './errorHandler.service';

// 14.6 Dodati dva metoda sa mokanim podacima ->app.component.html
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  //#region 36.2 Dodati logiku za uzimanje errora ->app.component.html
  private lastError: string[];
  //#endregion
  title = 'SportsStore';
  constructor(private repo: Repository, errorService: ErrorHandlerService) {
  //#region 36.2
  errorService.errors.subscribe(error => {
    this.lastError = error;
    });
  //#endregion
  }
  //#region 36.2
  get error(): string[] {
    return this.lastError;
    }
    clearError() {
    this.lastError = null;
    }
  //#endregion
  get product(): Product {
    return this.repo.product;
  }
  // 9. Dodati getProducts() ->app.component.html
  get products(): Product[] {
    return this.repo.products;
  }

  // 14.6
  // tslint:disable-next-line: typedef
  createProduct() {
    this.repo.createProduct(new Product(0, 'X-Ray Scuba Mask', 'Watersports',
      'See what the fish are hiding', 49.99, this.repo.products[0].supplier));
  }
  // tslint:disable-next-line: typedef
  createProductAndSupplier() {
    let s = new Supplier(0, 'Rocket Shoe Corp', 'Boston', 'MA');
    let p = new Product(0, 'Rocket-Powered Shoes', 'Running',
      'Set a new record', 100, s);
    this.repo.createProductAndSupplier(p, s);
  }
  //#region Dodati update metode u komponentu
  // 15.4 Dodati update metode u komponentu ->app.component.html
  // tslint:disable-next-line: typedef
  replaceProduct() {
    let p = this.repo.products[0];
    p.name = 'Modified Product';
    p.category = 'Modified Category';
    this.repo.replaceProduct(p);
  }
  // tslint:disable-next-line: typedef
  replaceSupplier() {
    let s = new Supplier(3, 'Modified Supplier', 'New York', 'NY');
    this.repo.replaceSupplier(s);
  }
  //#endregion Dodati update metode u komponentu

  //#region 16.5 Dodati patch u komponentu ->app.component.html
  // tslint:disable-next-line: typedef
  updateProduct() {
    let changes = new Map<string, any>();
    changes.set('name', 'Green Kayak');
    changes.set('supplier', null);
    this.repo.updateProduct(1, changes);
    }
  //#endregion

  //#region 17.5 dodati delete metode ->app.component.html
  // tslint:disable-next-line: typedef
  deleteProduct() {
    this.repo.deleteProduct(1);
  }
  // tslint:disable-next-line: typedef
  deleteSupplier() {
    this.repo.deleteSupplier(2);
  }
  //#endregion
}
