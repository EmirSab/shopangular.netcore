import { Component } from '@angular/core';
import { Cart } from '../models/cart.model';
@Component({
// tslint:disable-next-line: component-selector
selector: 'store-cartsummary',
templateUrl: 'cartSummary.component.html'
})
// 22.7 Napraviit summaryCart.ts ->cartSummary.component.html
export class CartSummaryComponent {
    //#region 27.3 dodati logiku -> cartSummary.component.html
    constructor(private cart: Cart) { }
    get itemCount(): number {
        return this.cart.itemCount;
    }
    get totalPrice(): number {
        return this.cart.totalPrice;
    }

    //#endregion
}
