import { Component } from '@angular/core';
import { Repository } from '../models/repository';
import { NavigationService } from '../models/navigation.service';
@Component({
// tslint:disable-next-line: component-selector
selector: 'store-categoryfilter',
templateUrl: 'categoryFilter.component.html'
})
// 22.5 Dodati komponetu za filtriranje kategorija ->
// 24.6 Updejtati komponentu NavigationService ->categoryFilter.component.html
export class CategoryFilterComponent {
    constructor(public service: NavigationService) { }
}
