import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CartSummaryComponent } from './cartSummary.component';
import { CategoryFilterComponent } from './categoryFilter.component';
import { PaginationComponent } from './pagination.component';
import { ProductListComponent } from './productList.component';
import { RatingsComponent } from './ratings.component';
import { ProductSelectionComponent } from './productSelection.component';
import { CartDetailComponent } from './cartDetail.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
//#region 31.13 dodati nove komponente ->app-routing.module.ts
import { CheckoutDetailsComponent } from './checkout/checkoutDetails.component';
import { CheckoutPaymentComponent } from './checkout/checkoutPayment.component';
import { CheckoutSummaryComponent } from './checkout/checkoutSummary.component';
import { OrderConfirmationComponent } from './checkout/orderConfirmation.component';
//#endregion

// 28.2 dodati novu komponentu kao i forms i router ->app-routing.module.ts
@NgModule({
    declarations: [CartSummaryComponent, CategoryFilterComponent,
        PaginationComponent, ProductListComponent, RatingsComponent, CheckoutDetailsComponent, CheckoutPaymentComponent,
        CheckoutSummaryComponent, OrderConfirmationComponent,
        ProductSelectionComponent, CartDetailComponent],
    imports: [BrowserModule, FormsModule, RouterModule],
    exports: [ProductSelectionComponent]
})
// 23 Napraviti file i dodati prethodno napravljene komponente ->app.module.ts
export class StoreModule { }
