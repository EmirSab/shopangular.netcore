import { Component } from '@angular/core';
import { Repository } from '../models/repository';
import { Product } from '../models/product.model';
import { Cart } from '../models/cart.model';
@Component({
    // tslint:disable-next-line: component-selector
    selector: 'store-product-list',
    templateUrl: 'productList.component.html'
})
    // 21.1 Dodati productList.component.ts ->productList.component.html
export class ProductListComponent {
    constructor(private repo: Repository, private cart: Cart) { }
    /*get products(): Product[] {
        return this.repo.products;
    }*/
    //#region 25.6 Dodati paginaciju na products
    get products(): Product[] {
        if (this.repo.products != null && this.repo.products.length > 0) {
            let pageIndex = (this.repo.paginationObject.currentPage - 1)
                * this.repo.paginationObject.productsPerPage;
            return this.repo.products.slice(pageIndex,
                pageIndex + this.repo.paginationObject.productsPerPage);
        }
    }
    //#endregion
    
    //#region 27.2 Dodati card i dodati addToCard() ->cartSummary.component.ts
    // tslint:disable-next-line: typedef
    addToCart(product: Product) {
        this.cart.addProduct(product);
        }
    //#endregion
}
