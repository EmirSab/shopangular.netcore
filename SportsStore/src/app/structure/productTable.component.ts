import { Component } from '@angular/core';
import { Repository } from '../models/repository';
import { Product } from '../models/product.model';
import { Router } from '@angular/router';
@Component({
    // tslint:disable-next-line: component-selector
    selector: 'product-table',
    templateUrl: 'productTable.component.html'
    })
    // 18.1 Napraviti productTable.component.ts ->productTable.component.html
export class ProductTableComponent {
    constructor(private repo: Repository, private router: Router) { }
    get products(): Product[] {
        return this.repo.products;
    }
    //#region 19.5 dodati selectProduct ->app-routing.module.ts
    // tslint:disable-next-line: typedef
    selectProduct(id: number) {
        this.repo.getProduct(id);
        // 20.2 dodati rutu da ide na detalje ->productDetail.component.html
        this.router.navigateByUrl('/detail');
        }
    //#endregion
}
