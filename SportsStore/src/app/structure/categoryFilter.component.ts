import { Component } from '@angular/core';
import { Repository } from '../models/repository';
@Component({
    selector: 'category-filter',
    templateUrl: 'categoryFilter.component.html'
})
// 18.3 dodati logiku za filtere
export class CategoryFilterComponent {
    public chessCategory = 'chess';
    constructor(private repo: Repository) { }
    // tslint:disable-next-line: typedef
    setCategory(category: string) {
        this.repo.filter.category = category;
        this.repo.getProducts();
    }
}
