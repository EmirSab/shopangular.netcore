import { Component } from '@angular/core';
import { Repository } from '../models/repository';
import { Product } from '../models/product.model';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
    selector: 'product-detail',
    templateUrl: 'productDetail.component.html'
})
// 19. Dodati komponentu za detalje ->productDetail.component.html
export class ProductDetailComponent {
    //#region 20.6 Dodavanje djela gdje uzima id producta u constructor ->productTable.html

    constructor(private repo: Repository,
                router: Router,
                activeRoute: ActivatedRoute) {
        // tslint:disable-next-line: radix
        let id = Number.parseInt(activeRoute.snapshot.params['id']);
        if (id) {
            this.repo.getProduct(id);
        } else {
            router.navigateByUrl('/');
        }
    }
    //#endregion
    get product(): Product {
        return this.repo.product;
    }
}
