//#region 33.5 dodati admin dio za ordere ->orderAdmin.component.html
import { Component } from '@angular/core';
import { Repository } from '../models/repository';
import { Order } from '../models/order.model';
@Component({
    templateUrl: 'orderAdmin.component.html'
})
export class OrderAdminComponent {
    //#region 35 Dodati logiku za ordere -> orderAdmin.component.html
    constructor(private repo: Repository) { }
    get orders(): Order[] {
        return this.repo.orders;
    }
    // tslint:disable-next-line: typedef
    markShipped(order: Order) {
        this.repo.shipOrder(order);
    }
    //#endregion
}
//#endregion
