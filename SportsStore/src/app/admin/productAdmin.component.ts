//#region 33.4 dodati komponentu za managovanje produkata -> productAdmin.component.html
import { Component } from '@angular/core';
import { Repository } from '../models/repository';
import { Product } from '../models/product.model';
@Component({
    templateUrl: 'productAdmin.component.html'
})
export class ProductAdminComponent {
    //#region 34.3 Dodati logiku za producte ->productAdmin.component.html
    constructor(private repo: Repository) { }
    // tslint:disable-next-line: no-inferrable-types
    tableMode: boolean = true;
    get product(): Product {
        return this.repo.product;
    }
    // tslint:disable-next-line: typedef
    selectProduct(id: number) {
        this.repo.getProduct(id);
    }
    // tslint:disable-next-line: typedef
    saveProduct() {
        if (this.repo.product.productId == null) {
            this.repo.createProduct(this.repo.product);
        } else {
            this.repo.replaceProduct(this.repo.product);
        }
        this.clearProduct();
        this.tableMode = true;
    }
    // tslint:disable-next-line: typedef
    deleteProduct(id: number) {
        this.repo.deleteProduct(id);
    }
    // tslint:disable-next-line: typedef
    clearProduct() {
        this.repo.product = new Product();
        this.tableMode = true;
    }
    get products(): Product[] {
        return this.repo.products;
    }
    //#endregion
}
//#endregion
