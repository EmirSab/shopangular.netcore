//#region 33.8 napraviti admin module da se dodaju komponente ->app-routing.module.ts
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AdminComponent } from './admin.component';
import { OverviewComponent } from './overview.component';
import { ProductAdminComponent } from './productAdmin.component';
import { OrderAdminComponent } from './orderAdmin.component';
//#region 34.2 Importovati productEditor i common module ->productAdmin.component.ts
import { ProductEditorComponent } from './productEditor.component';
import { CommonModule } from '@angular/common';
import { AuthModule } from '../auth/auth.module';
import { AuthenticationComponent } from '../auth/authentication.component';
import { AuthenticationGuard } from '../auth/authentication.guard';
//#endregion

// 39.2 Dodati admin module, guard i komponentu ->admin.component.ts
const routes: Routes = [
    // 39.2
    { path: 'login', component: AuthenticationComponent },
    {
        path: '', component: AdminComponent,
        // 39.2
        canActivateChild: [AuthenticationGuard],
        children: [
            { path: 'products', component: ProductAdminComponent },
            { path: 'orders', component: OrderAdminComponent },
            { path: 'overview', component: OverviewComponent },
            { path: '', component: OverviewComponent }]
    }
];
@NgModule({
    imports: [RouterModule,
        FormsModule, RouterModule.forChild(routes), CommonModule, AuthModule],
    declarations: [AdminComponent, OverviewComponent,
        ProductAdminComponent, OrderAdminComponent, ProductEditorComponent]
})
export class AdminModule { }
//#endregion
