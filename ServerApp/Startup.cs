using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using ServerApp.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

namespace ServerApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            string connectionString = Configuration["ConnectionStrings:DefaultConnection"];
            services.AddDbContext<DataContext>(options => options.UseSqlServer(connectionString));

            #region 38.2 DOdati dio za autorizaciju ->appsettings.Development.json ->AccountsController
            string dconnectionString = Configuration["ConnectionStrings:Identity"];
            services.AddDbContext<IdentityDataContext>(options => options.UseSqlServer(dconnectionString));
            services.AddIdentity<IdentityUser, IdentityRole>()
            .AddEntityFrameworkStores<IdentityDataContext>();
            #endregion
            // 5. iZbjegavati slanje NULL vrijednosti ->app.componenet.html
            // 16.3 Dodati newtonsoftjson radi parsiranja podataka  ->repository.ts
            services.AddControllersWithViews().AddJsonOptions(opts =>
            {
                opts.JsonSerializerOptions.IgnoreNullValues = true;
            }).AddNewtonsoftJson();

            #region 29.1 dodati dio za session storage
            services.AddDistributedSqlServerCache(options =>
            {
                options.ConnectionString = connectionString;
                options.SchemaName = "dbo";
                options.TableName = "SessionData";
            });
            services.AddSession(options =>
            {
                options.Cookie.Name = "SportsStore.Session";
                options.IdleTimeout = System.TimeSpan.FromHours(48);
                options.Cookie.HttpOnly = false;
                options.Cookie.IsEssential = true;
            });
            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider services)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            // 29.1
            app.UseSession();
            app.UseRouting();
            #region 38.2
            app.UseAuthentication();
            app.UseAuthorization();
            #endregion

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                // 20.4 dodati dio kada nestane bootstrapa ->app-routing.ts
                endpoints.MapControllerRoute(
                    name: "angular_fallback",
                    // 23.3 prepraviti pattern _Layout.cshtml
                    //pattern: "{target:regex(table|detail)}/{*catchall}",
                    //pattern: "{target:regex(store)}/{*catchall}",
                    // 28.4 prepravka patterna ponovo ->ProductSelection.cs
                    //pattern: "{target:regex(store|cart)}/{*catchall}",
                    //31.15 modifikovati fallback rutu ->CheckoutState
                    //pattern: "{target:regex(store|cart|checkout)}/{*catchall}",
                    //33.11 Prrepraviti pattern ->productEditor.component.ts
                    pattern: "{target:regex(admin|store|cart|checkout):nonfile}/{*catchall}",
                    defaults: new { controller = "Home", action = "Index" });
            });
            /*
            app.UseEndpoints(endpoints => {
                endpoints.MapControllerRoute(
                name: "default",
                pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
                });
            */
            // instalirati dotnet add package Microsoft.AspNetCore.SpaServices.Extensions
            app.UseSpa(spa =>
            {
                string strategy = Configuration
                .GetValue<string>("DevTools:ConnectionStrategy");
                if (strategy == "proxy")
                {
                    spa.UseProxyToSpaDevelopmentServer("http://127.0.0.1:4200");
                }
                else if (strategy == "managed")
                {
                    spa.Options.SourcePath = "../SportsStore";
                    spa.UseAngularCliServer("start");
                }
            });
            SeedData.SeedDatabase(services.GetRequiredService<DataContext>());
            //38.2
            IdentitySeedData.SeedDatabase(services).Wait();
        }
    }
}
