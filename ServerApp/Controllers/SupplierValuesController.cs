using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ServerApp.Models;
using ServerApp.Models.BindingTargets;

namespace ServerApp.Controllers
{
    // 14.4 Napraviti Controller za Supplies i dodati metode ->repository.ts
    [Route("api/suppliers")]
    //37 Dodati taga da samo administrator moze pristupiti ->OrderValuesController.cs
    [Authorize(Roles = "Administrator")]
    public class SupplierValuesController : Controller
    {
        private DataContext context;
        public SupplierValuesController(DataContext ctx)
        {
            context = ctx;
        }
        [HttpGet]
        public IEnumerable<Supplier> GetSuppliers()
        {
            return context.Suppliers;
        }
        [HttpPost]
        public IActionResult CreateSupplier([FromBody] SupplierData sdata)
        {
            if (ModelState.IsValid)
            {
                Supplier s = sdata.Supplier;
                context.Add(s);
                context.SaveChanges();
                return Ok(s.SupplierId);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        //15.2 Dodati update metod za supplier ->repository.ts
        [HttpPut("{id}")]
        public IActionResult ReplaceSupplier(long id, [FromBody] SupplierData sdata)
        {
            if (ModelState.IsValid)
            {
                Supplier s = sdata.Supplier;
                s.SupplierId = id;
                context.Update(s);
                context.SaveChanges();
                return Ok();
            }
            else
            {
                return BadRequest(ModelState);
            }
        }
        #region 17.3 Dodati delete za supplies -repository.ts
        [HttpDelete("{id}")]
        public void DeleteSupplier(long id)
        {
            context.Remove(new Supplier { SupplierId = id });
            context.SaveChanges();
        }
        #endregion
    }
}