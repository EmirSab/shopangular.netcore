namespace ServerApp.Models.BindingTargets
{
    public class CheckoutState
    {
        #region 32 Dodati model za provjeru statea ->SessionValuesController
        public string name { get; set; }
        public string address { get; set; }
        public string cardNumber { get; set; }
        public string cardExpiry { get; set; }
        public string cardSecurityCode { get; set; }
        #endregion
    }
}