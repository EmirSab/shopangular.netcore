namespace ServerApp.Models
{
    // 29 Dodati model za spasavanje cart dodati sljedece ->Startup.cs
    //dotnet add package Microsoft.Extensions.Caching.SqlServer
    //dotnet tool uninstall --global dotnet-sql-cache
    //dotnet tool install --global dotnet-sql-cache
    //dotnet sql-cache create "Server=DESKTOP-7AOGO77\EMIR;Database=EssentialApp;Trusted_Connection=True" "dbo" "SessionData" 
    public class ProductSelection
    {
        public long productId { get; set; }
        public string name { get; set; }
        public decimal price { get; set; }
        public int quantity { get; set; }
    }
}